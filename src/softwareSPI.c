
#include <stdio.h>
#include <string.h>
#include "freertos\FreeRTOS.h"
#include "freertos\task.h"
#include "driver\gpio.h"
#include "softwareSPI.h"
#include "pn532.h"



byte pn532ack[] = {0x00, 0x00, 0xFF, 0x00, 0xFF, 0x00};
byte pn532response_firmwarevers[] = {0x00, 0xFF, 0x06, 0xFA, 0xD5, 0x03};

#define PN532_PACKBUFFSIZ 64
byte pn532_packetbuffer[PN532_PACKBUFFSIZ];

#ifndef _BV
    #define _BV(bit) (1<<(bit))
#endif

uint8_t _miso, _mosi, _clk, _ss;
void begin();

void pn532Init(uint8_t clk, uint8_t miso, uint8_t mosi, uint8_t ss){
    _miso = miso;
    _mosi = mosi;
    _clk = clk;
    _ss = ss;
    set_direction(_ss, GPIO_MODE_OUTPUT);
    set_level(_ss, 1); 
    set_direction(_clk, GPIO_MODE_OUTPUT);
    set_direction(_mosi, GPIO_MODE_OUTPUT);
    set_direction(_miso, GPIO_MODE_INPUT);
    begin();
}

void spi_write(uint8_t c){
    int8_t i;
    set_level(_clk, 1);

    for (i=0; i<8; i++) {
      set_level(_clk, 0);
      if (c & _BV(i)) {
        set_level(_mosi, 1);
      } else {
        set_level(_mosi, 0);
      }
      set_level(_clk, 1);
    }
}

uint8_t spi_read(){
    int8_t i, x;
    x = 0;

    set_level(_clk, 1);

    for (i=0; i<8; i++) {
      if (get_level(_miso)) {
        x |= _BV(i);
      }
      set_level(_clk, 0);
      set_level(_clk, 1);
    }
    return x;
}

void writecommand(uint8_t* cmd, uint8_t cmdlen){
    uint8_t checksum;

    cmdlen++;

    set_level(_ss, 0);
    wait(2);     // or whatever the wait is for waking up the board
    spi_write(PN532_SPI_DATAWRITE);

     checksum = PN532_PREAMBLE + PN532_PREAMBLE + PN532_STARTCODE2;
    spi_write(PN532_PREAMBLE);
    spi_write(PN532_PREAMBLE);
    spi_write(PN532_STARTCODE2);

    spi_write(cmdlen);
    spi_write(~cmdlen + 1);

    spi_write(PN532_HOSTTOPN532);
    checksum += PN532_HOSTTOPN532;

    for (uint8_t i=0; i<cmdlen-1; i++) {
      spi_write(cmd[i]);
      checksum += cmd[i];
    }

    spi_write(~checksum);
    spi_write(PN532_POSTAMBLE);
    set_level(_ss, 1);
}

void readdata(uint8_t* buff, uint8_t n){
    set_level(_ss, 0);
    wait(2);
    spi_write(PN532_SPI_DATAREAD);

    for (uint8_t i=0; i<n; i++) {
      wait(1);
      buff[i] = spi_read();
    }
   
    set_level(_ss, 1);
}

bool isready(){
    set_level(_ss, 0);
    wait(2);
    spi_write(PN532_SPI_STATREAD);
    // read byte
    uint8_t x = spi_read();

    set_level(_ss, 1);

    // Check if status is ready.
    return x == PN532_SPI_READY;
}

bool waitready(uint16_t timeout){
   uint16_t timer = 0;
   while(!isready()) {
    if (timeout != 0) {
      timer += 10;
      if (timer > timeout) {
        return false;
      }
    }
    wait(10);
  }
  return true;
}

bool readack() {
  uint8_t ackbuff[6];

  readdata(ackbuff, 6);

  return (0 == strncmp((char *)ackbuff, (char *)pn532ack, 6));
}

bool sendCommandCheckAck(uint8_t *cmd, uint8_t cmdlen) {
    
    uint16_t timeout = TIMEOUT;
    // write the command
    writecommand(cmd, cmdlen);

    // Wait for chip to say its ready!
    if (!waitready(timeout)) {
        printf("waitready failed\n");
        return false;
    }

     if (!readack()) {
         printf("readack failed\n");
         return false;
     }

      if (!waitready(timeout)) {
      return false;
    }

    return true;
}

uint32_t getFirmwareVersion(void) {
    uint32_t response;

    pn532_packetbuffer[0] = PN532_COMMAND_GETFIRMWAREVERSION;

    if (! sendCommandCheckAck(pn532_packetbuffer, 1)) {
        printf("I went off here\n");
        return 0;
    }
    // read data packet
    readdata(pn532_packetbuffer, 12);

    // check some basic stuff
    if (0 != strncmp((char *)pn532_packetbuffer, (char *)pn532response_firmwarevers, 6)) {
        return 0;
    }

    int offset = 6;

    response = pn532_packetbuffer[offset++];
    response <<= 8;
    response |= pn532_packetbuffer[offset++];
    response <<= 8;
    response |= pn532_packetbuffer[offset++];
    response <<= 8;
    response |= pn532_packetbuffer[offset++];

  return response;
}

void begin(){
    set_level(_ss, 0);
    wait(1000);
    pn532_packetbuffer[0] = PN532_COMMAND_GETFIRMWAREVERSION;
    sendCommandCheckAck(pn532_packetbuffer, 1);
    set_level(_ss, 1);
}


bool SAMConfig(void) {
  pn532_packetbuffer[0] = PN532_COMMAND_SAMCONFIGURATION;
  pn532_packetbuffer[1] = 0x01; // normal mode;
  pn532_packetbuffer[2] = 0x14; // timeout 50ms * 20 = 1 second
  pn532_packetbuffer[3] = 0x01; // use IRQ pin!

  if (! sendCommandCheckAck(pn532_packetbuffer, 4))
    return false;

  // read data packet
  readdata(pn532_packetbuffer, 8);

  int offset = 5;
  return  (pn532_packetbuffer[offset] == 0x15);
}

bool readPassiveTargetID(uint8_t cardbaudrate, uint8_t * uid, uint8_t * uidLength) {
  uint16_t timeout = TIMEOUT;
  pn532_packetbuffer[0] = PN532_COMMAND_INLISTPASSIVETARGET;
  pn532_packetbuffer[1] = 1;  // max 1 cards at once (we can set this to 2 later)
  pn532_packetbuffer[2] = cardbaudrate;

  if (!sendCommandCheckAck(pn532_packetbuffer, 3))
  {
    return 0x0;  // no cards read
  }

  // wait for a card to enter the field (only possible with I2C)
 
    if (!waitready(timeout)) {
     
      return 0x0;
    }

  // read data packet
  readdata(pn532_packetbuffer, 20);
  // check some basic stuff

  /* ISO14443A card response should be in the fol0ing format:

    byte            Description
    -------------   ------------------------------------------
    b0..6           Frame header and preamble
    b7              Tags Found
    b8              Tag Number (only one used in this example)
    b9..10          SENS_RES
    b11             SEL_RES
    b12             NFCID Length
    b13..NFCIDLen   NFCID                                      */

  
  if (pn532_packetbuffer[7] != 1)
    return 0;

  uint16_t sens_res = pn532_packetbuffer[9];
  sens_res <<= 8;
  sens_res |= pn532_packetbuffer[10];
  
  /* Card appears to be Mifare Classic */
  *uidLength = pn532_packetbuffer[12];
  
  for (uint8_t i=0; i < pn532_packetbuffer[12]; i++)
  {
    uid[i] = pn532_packetbuffer[13+i];
   
  }

  return 1;
}


bool testTarget(byte* u8_UidBuffer, byte* pu8_UidLength) 
{
   
    *pu8_UidLength = 0;
    memset(u8_UidBuffer, 0, 8);
      
    pn532_packetbuffer[0] = PN532_COMMAND_INLISTPASSIVETARGET;
    pn532_packetbuffer[1] = 1;  // read data of 1 card (The PN532 can read max 2 targets at the same time)
    pn532_packetbuffer[2] = 0x00; //CARD_TYPE_106KB_ISO14443A; // This function currently does not support other card types.
  
    if (!sendCommandCheckAck(pn532_packetbuffer, 3))
        return false; // Error (no valid ACK received or timeout)
  
    /* 
    ISO14443A card response:
    mu8_PacketBuffer Description
    -------------------------------------------------------
    b0               D5 (always) (PN532_PN532TOHOST)
    b1               4B (always) (PN532_COMMAND_INLISTPASSIVETARGET + 1)
    b2               Amount of cards found
    b3               Tag number (always 1)
    b4,5             SENS_RES (ATQA = Answer to Request Type A)
    b6               SEL_RES  (SAK  = Select Acknowledge)
    b7               UID Length
    b8..Length       UID (4 or 7 bytes)
    nn               ATS Length     (Desfire only)
    nn..Length-1     ATS data bytes (Desfire only)
    */ 
    readdata(pn532_packetbuffer, 28);
    if (pn532_packetbuffer[1] != PN532_COMMAND_INLISTPASSIVETARGET + 1)
    {
        return false;
    }   

    byte cardsFound = pn532_packetbuffer[2]; 
   
    if (cardsFound != 1)
        return true; // no card found -> this is not an error!

    byte u8_IdLength = pn532_packetbuffer[7];
    if (u8_IdLength != 4 && u8_IdLength != 7)
    {
        return true; // unsupported card found -> this is not an error!
    }   

    memcpy(u8_UidBuffer, pn532_packetbuffer + 8, u8_IdLength);    
    *pu8_UidLength = u8_IdLength;

    // See "Mifare Identification & Card Types.pdf" in the ZIP file
    uint16_t u16_ATQA = ((uint16_t)pn532_packetbuffer[4] << 8) | pn532_packetbuffer[5];
    byte     u8_SAK   = pn532_packetbuffer[6];

    // if (u8_IdLength == 7 && u8_UidBuffer[0] != 0x80 && u16_ATQA == 0x0344 && u8_SAK == 0x20) *pe_CardType = CARD_Desfire;
    // if (u8_IdLength == 4 && u8_UidBuffer[0] == 0x80 && u16_ATQA == 0x0304 && u8_SAK == 0x20) *pe_CardType = CARD_DesRandom;
    
    return true;
}
