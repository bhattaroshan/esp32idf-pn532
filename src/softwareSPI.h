

#define set_direction(pin, mode) gpio_set_direction(pin, mode)
#define set_level(pin,state) gpio_set_level(pin,state)
#define get_level(pin) gpio_get_level(pin)

#define byte uint8_t 
#define wait(milliseconds) vTaskDelay(milliseconds/portTICK_PERIOD_MS)
#define TIMEOUT 1000

void begin();
void pn532Init(uint8_t clk, uint8_t miso, uint8_t mosi, uint8_t ss);
uint32_t getFirmwareVersion(void);
bool readPassiveTargetID(uint8_t cardbaudrate, uint8_t * uid, uint8_t * uidLength);
bool SAMConfig(void);


bool testTarget(byte* u8_UidBuffer, byte* pu8_UidLength);