
#include <stdio.h>
#include "freertos\FreeRTOS.h"
#include "freertos\task.h"
#include "softwareSPI.h"
#include "pn532.h"


void app_main(){
    // pn532Init(uint8_t sck, uint8_t miso, uint8_t mosi, uint8_t ss){
    pn532Init(18,19,23,5);
    uint32_t versiondata = getFirmwareVersion();

    if (! versiondata) {
        printf("Didn't find PN53x board\n");
    while (1){
            printf("Didn't find PN53x board\n");
            wait(1000); 
        }
    }

    SAMConfig();

    while(1){
        // printf("Found chip PN5 %d\n",((versiondata>>24) & 0xFF)); 
        // printf("Firmware ver. %d\n",((versiondata>>16) & 0xFF)); 
        // printf("%d\n",((versiondata>>8) & 0xFF));
        // vTaskDelay(1000/portTICK_PERIOD_MS);
        uint8_t success;
        uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
        uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
        
        // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
        // 'uid' will be populated with the UID, and uidLength will indicate
        // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
        success = readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);

        // success = testTarget(uid, &uidLength);        
        if(success){
            for(int i=0; i<7; ++i){
                printf("%d, ",uid[i]);
            }
            printf("\n");
        }
  
    }
}

